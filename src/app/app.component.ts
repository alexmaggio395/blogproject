import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'firstBlog';

  posts = [
    {
      title: 'Mon premier post',
      content: 'bla',
      loveIts: -1,
      created_at: new Date()
    },
    {
      title: 'Mon deuxième post',
      content: 'blabla',
      loveIts: 0,
      created_at: new Date()
    },
    {
      title: 'Encore un post',
      content: 'blablabla',
      loveIts: 3,
      created_at: new Date()
    }
  ];

}
